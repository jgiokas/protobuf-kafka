package org.example.messaging;

import lombok.extern.slf4j.Slf4j;
import org.example.Event;
import org.example.Other;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.annotation.KafkaHandler;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

@Service
@EnableKafka
@KafkaListener(topics = "test")
@Slf4j
public class MessageConsumer {

    @KafkaHandler
    public void handleEvent(
            @Payload Event event,
            @Header(name = KafkaHeaders.RECEIVED_MESSAGE_KEY, required = false) String key
    ) {
        log.info("Key: {}, Value: {}", key, event.toString());
    }

    @KafkaHandler
    public void handleOther(
            @Payload Other other,
            @Header(name = KafkaHeaders.RECEIVED_MESSAGE_KEY, required = false) String key
    ) {
        log.info("Key: {}, Value: {}", key, other.toString());
    }
}
