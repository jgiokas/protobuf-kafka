package org.example.controllers;

import org.example.Event;
import org.example.Other;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
public class ProducerController {

    private final KafkaTemplate<String, Event> eventKafkaTemplate;
    private final KafkaTemplate<String, Other> otherKafkaTemplate;

    @Autowired
    public ProducerController(
            KafkaTemplate<String, Event> eventKafkaTemplate,
            KafkaTemplate<String, Other> otherKafkaTemplate
    ) {
        this.eventKafkaTemplate = eventKafkaTemplate;
        this.otherKafkaTemplate = otherKafkaTemplate;
    }

    @PostMapping("/event")
    public ResponseEntity<?> createEvent() {
        Event e = Event.newBuilder()
                .setId(UUID.randomUUID().toString())
                .setType("event_type")
                .build();

        eventKafkaTemplate.send("test", "1", e);

        return ResponseEntity.ok().build();
    }

    @PostMapping("/other")
    public ResponseEntity<?> createOther() {
        Other o = Other.newBuilder()
                .setId((long) (Math.random() * 10L))
                .setPayload("blah blah")
                .build();

        otherKafkaTemplate.send("test", "1", o);

        return ResponseEntity.ok().build();
    }
}
